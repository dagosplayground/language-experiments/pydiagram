from diagrams import Cluster, Diagram, Edge
from diagrams.onprem.compute import Server
from diagrams.onprem.network import Internet
from diagrams.generic.network import Router
from diagrams.generic.network import Switch
from diagrams.k8s.infra import Node
from diagrams.k8s.infra import Master
from diagrams.custom import Custom


with Diagram(name="BuzzCrate", direction="LR") as diag:
	outside = Internet("Cocmast")
	router = Router("Asus Gaming Router")
	mesh = Switch("Trendnet 8port gig")
	builderArm = Server("Odroid N2 Builder")
	builderAmd = Server("Intel Xeon Builder")
    # Download first locally from https://about.gitlab.com/images/press/logo/png/gitlab-logo-gray-stacked-rgb.png
	gitlab = Custom("GitLab CI/CD", "gitlab.png")


	with Cluster("Odroid C2s"):
		kluster = list()
		kluster.append(Master("Controller 1"))
		for i in range(1,5):
			kluster.append(Node("Worker "+str(i))) 

	outside - router >> Edge(color="blue") >> mesh
	mesh >> Edge(color="green") >>  kluster
	router >> Edge(color="blue") >> builderArm
	router >> Edge(color="blue") >> builderAmd
	outside - gitlab
