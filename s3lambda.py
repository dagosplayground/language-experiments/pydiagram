from diagrams import Diagram, Cluster, Edge
from diagrams.aws.network import ElbApplicationLoadBalancer as ALB
from diagrams.aws.storage import S3
from diagrams.aws.compute import LambdaFunction
from diagrams.aws.management import Cloudwatch
from diagrams.programming.language import Go

with Diagram("ALB Event Logger for CloudWatch", direction='LR') as diag:
	albEvents = Cloudwatch("ALB Log Events")
	albLogs = S3("Combined ALB Logs")
	lambda_function = LambdaFunction("S3 Create")
	lambda_code = Go("Parse, Json, Push")
	with Cluster("System Ingress"):
		ingress_controllers = [
			ALB("us-east-1"),
			ALB("us-east-2")
			]
		pass

	ingress_controllers >> albLogs
	albLogs >> Edge(label="S3 Create", color="green") >> lambda_function
	lambda_function >> lambda_code
	lambda_code >> albEvents
	
